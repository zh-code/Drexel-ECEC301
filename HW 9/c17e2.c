#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char *duplicate(const char *s)
{
  char *temp = malloc(strlen(s) + 1);

  if (temp == NULL)
    return NULL;

  strcpy(temp, s);
  return temp;
}

int main (int argc, char** argv)
{
	const char *str;
	char *p;
	
	str = "ECEC301";
	
	p = duplicate(str);
	
	printf ("%s\n", p);
	
}