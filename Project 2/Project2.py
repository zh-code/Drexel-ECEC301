import turtle
import fractions
import math

class Spirograph(object):

    def __init__(self, R):
        self.R = float(R) #Set R: bigger circle radius
        self.turtle = turtle #Set turtle to self will be used later.

    def setSmallCircle(self, r):
        self.r = float(r)#Set r: smaller circle radius

    def setPen(self, l, color):
        self.l = float(l)# l = d/r. l: distance between pen point and center.
        self.turtle.color(color) #set pen color.

    def draw(self):
        k = self.r/self.R #Calculate k. k is used in calculation
        #simplify the fraction number to get how much circles turtle will go
        f = fractions.Fraction(int(self.r), int(self.R))
        p_b = f.numerator #returns numerator value
        p_s = f.denominator #returns denomenator value
        t = math.pi * 2
        item = 0 #initial value for theta
        
        self.turtle.up()
        #Set maximum number for theta and the turtle will not go any further after complete the graph.
        while item < t*p_b:
            x = self.cal_x (self.R, self.l, k, item) #cal_x
            y = self.cal_y (self.R, self.l, k, item) #cal_
            item = item + math.pi/180 #increase theta each loop by pi/180
            self.turtle.goto(x, y) #let turtle go to poin
            self.turtle.down() #put pen down. Turtle keep drawing.
        self.turtle.up()# finish drawing, move pen up.
        
    def clear(self):
        self.turtle.clear() # clear figure.

    def cal_x(self, R, l, k, t):
        #formula to calculate x position
        X = R * ((1 - k) * math.cos(t) + l * k * math.cos(((1 - k) / k) * t))
        return X
        
    def cal_y(self, R, l, k, t):
        #formula to calculate y position
        Y = R * ((1 - k) * math.sin(t) - l * k * math.sin(((1 - k) / k) * t))
        return Y

    
