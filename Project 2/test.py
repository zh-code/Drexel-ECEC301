from Project2 import *

a = Spirograph(500)
a.setSmallCircle(85)
a.setPen(0.65, 'red')
a.draw()
a.setSmallCircle(120)
a.setPen(0.22, 'red')
a.draw()

a.clear()
a.setSmallCircle(20)
a.setPen(0.8, 'red')
a.draw()
