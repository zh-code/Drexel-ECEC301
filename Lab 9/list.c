#include <stdlib.h>
#include "list.h"

void list_init (struct list* head){
	head->next = NULL;
}

unsigned int list_count (struct list* head){
	struct list* item = head;
	int n = 0;
	
	while (item->next){
		n++;
		item = item->next;
	}
	return n;
}

void list_add_to_head (struct list* head, struct list* new_item){
	new_item->next = head->next;
	head->next = new_item;
}

struct list* list_pop_head (struct list* head){
	struct list* item;
	item = head->next;
	if (item != NULL) {
		head->next = item->next;
		item->next = NULL;
	}
	return item;
}