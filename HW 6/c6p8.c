#include<stdio.h>
int main(void){
    int i, n, d;
    printf("Enter number of days in month:");
    scanf("%d", &n);
    printf("Enter starting day of the week (1=sun, 7=Sat): ");
    scanf("%d", &d);
    for (i = 1; i < d; i++){
	printf("   ");
    }
    for (i = 1; i <= n; i++){
	printf("%3d", i);
	if (d == 7)
	{
	    printf("\n");
	    d = 1;
	}
	else{
	    d = d + 1;
	}
    }
    printf("\n");
}