#!/usr/bin/python
"""
 Author: James A. Shackleford
   Date: Oct. 16th, 2015

   A simple implementation of Conway's Game of Life
"""
import patterns
import random
import sys
import argparse
from matplotlib import pyplot as plt
from matplotlib import animation


def generate_world(opts):
    """
    Accepts: opts  -- parsed command line options
    Returns: world -- a list of lists that forms a 2D pixel buffer

    Description: This function generates a 2D pixel buffer with dimensions
                 opts.cols x opts.rows (in pixels).  The initial contents
                 of the generated world is determined by the value provided
                 by opts.world_type: either 'random' or 'empty'  A 'random'
                 world has 10% 'living' pixels and 90% 'dead' pixels.  An
                 'empty' world has 100% 'dead' pixels.
    """
    world = []

    ## TASK 1 #############################################################
    #
    #                    [ YOUR CODE GOES HERE ]
    if opts.world_type == 'empty':
        for x in range (0, opts.rows):
            line = [0] * (opts.cols)
            world.append(line)
    elif opts.world_type == 'random':
        for x in range(0, opts.rows):
            line = []
            for y in range(0, opts.cols):
                r = random.random()
                if r >= 0.1:
                    line.append(0)
                else:
                    line.append(1)
            world.append(line)
    #
    #######################################################################

    return world


def update_frame(frame_num, opts, world, img):
    """
    Accepts: frame_num  -- (automatically passed in) current frame number
             opts       -- a populated command line options instance
             world      -- the 2D world pixel buffer
             img        -- the plot image
    """

    # set the current plot image to display the current 2D world matrix
    img.set_array(world)

    # Create a *copy* of 'world' called 'new_world' -- 'new_world' will be
    # our offscreen drawing buffer.  We will draw the next frame to
    # 'new_world' so that we may maintain an in-tact copy of the current
    # 'world' at the same time.
    new_world = []
    for row in world:
        new_world.append(row[:])

    ## TASK 3 #############################################################
    #
    #                    [ YOUR CODE GOES HERE ]
    #
    for row in range (0, opts.rows-1):
        for col in range (0, opts.cols-1):
            count_live = 0
            row_check = [row-1, row, row+1]
            col_check = [col-1, col, col+1]
            # If a cell is alive and has less than 2 and more than 3 dies. Underpopulation and Overpopulation
            if world[row][col] > 0:
                for r in row_check:
                    for c in col_check:
                        if world[r][c] > 0:
                            count_live += 1
                count_live -= 1
                if count_live < 2 or count_live > 3:
                    new_world[row][col] = 0
                            
            else:
                for r in row_check:
                    for c in col_check:
                        if world[r][c] > 0:
                            count_live += 1
                if count_live == 3:
                    new_world[row][col] = 1
    #######################################################################

    # Copy the contents of the new_world into the world
    # (i.e. make the future the present)
    world[:] = new_world[:]
    return img,


def blit(world, sprite, x, y):
    """
    Accepts: world  -- a 2D world pixel buffer generated by generate_world()
             sprite -- a 2D matrix containing a pattern of 1s and 0s
             x      -- x world coord where left edge of sprite will be placed
             y      -- y world coord where top edge of sprite will be placed

    Returns: (Nothing)

    Description: Copies a 2D pixel pattern (i.e sprite) into the larger 2D
                 world.  The sprite will be copied into the 2D world with
                 its top left corner being located at world coordinate (x,y)
    """
    ## TASK 2 #############################################################
    #
    #                    [ YOUR CODE GOES HERE ]
    #
    r = x - 1
    for row in sprite:
        c = y - 1
        for item in row:
            world[r][c] = item
            c += 1
        r += 1
    #######################################################################


def run_simulation(opts, world):
    """
    Accepts: opts  -- a populated command line options class instance
             world -- a 2D world pixel buffer generated by generate_world()

    Returns: (Nothing)

    Description: This function generates the plot that we will use as a
                 rendering surfance.  'Living' cells (represented as 1s in
                 the 2D world matrix) will be rendered as black pixels and
                 'dead' cells (represetned as 0s) will be rendered as
                 white pixels.  The method FuncAnimation() accepts 4
                 parameters: the figure, the frame update function, a
                 tuple containing arguments to pass to the update function,
                 and the frame update interval (in milliseconds).  Once the
                 show() method is called to display the plot, the frame
                 update function will be called every 'interval'
                 milliseconds to update the plot image (img).
    """
    if not world:
        print "The 'world' was never created.  Exiting"
        sys.exit()

    fig = plt.figure()
    img = plt.imshow(world, interpolation='none', cmap='Greys', vmax=1, vmin=0)
    ani = animation.FuncAnimation(fig,
                                  update_frame,
                                  fargs=(opts, world, img),
                                  interval=opts.framedelay)

    plt.show()


def report_options(opts):
    """
    Accepts: opts  -- a populated command line options class instance

    Returns: (Nothing)

    Descrption: This function simply prints the parameters used to
                start the 'Game of Life' simulation.
    """

    print "Conway's Game of Life"
    print "====================="
    print "   World Size: %i x %i" % (opts.rows, opts.cols)
    print "   World Type: %s" % (opts.world_type)
    print "  Frame Delay: %i (ms)" % (opts.framedelay)


def get_commandline_options():
    """
    Accepts: (Nothing)

    Returns: opts  -- an instance of the options class that possesses members
                      specified by the 'dest' parameter of the add_option()
                      method.  Members contain the 'default' value unless
                      the user supplies a value from the command line using
                      the appropriate switch (i.e. '-r 100' or '--rows 100')

    optparse module documentation:
    https://docs.python.org/2/library/optparse.html
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('-r', '--rows',
                        help='set # of rows in the world',
                        action='store',
                        type=int,
                        dest='rows',
                        default=50)

    parser.add_argument('-c', '--columns',
                        help='set # of columns in the world',
                        action='store',
                        type=int,
                        dest='cols',
                        default=50)

    parser.add_argument('-w', '--world',
                        help='type of world to generate',
                        action='store',
                        type=str,
                        dest='world_type',
                        default='empty')

    parser.add_argument('-d', '--framedelay',
                        help='time (in milliseconds) between frames',
                        action='store',
                        type=int,
                        dest='framedelay',
                        default=100)

    opts = parser.parse_args()

    return opts


def main():
    """
    The main function -- everything starts here!
    """
    opts = get_commandline_options()
    world = generate_world(opts)
    report_options(opts)

    blit(world, patterns.glider, 20, 20)

    run_simulation(opts, world)


if __name__ == '__main__':
    main()
