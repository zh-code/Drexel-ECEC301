#include <stdlib.h>
#include <stdio.h>

struct list {
	unsigned int a;
	
	struct list* next;
};

void add_item (struct list* head, struct list* new_item){
	new_item->next = head->next;
	head->next = new_item;
}

struct list* pop_item (struct list* head){
	struct list* item;
	item = head->next;
	if (item != NULL) {
		head->next = item->next;
		item->next = NULL;
	}
	return item;
}


int main (int argc, char** argv){
	struct list* head;
	struct list* item;
	
	unsigned int i = 0;
	
	head = malloc (sizeof(struct list));
	
	head->next = NULL;
	
	for (i=0; i< 5; i++){
		item = malloc(sizeof(struct list));
		item->a = i;
		add_item(head, item);
	}
	
	for (i=0, item=head->next; item != NULL; item = item->next){
		printf ("  a = %u\n", item->a);
		
	}
	
	for (i=0, item = pop_item(head); item != NULL; item = pop_item(head)){
		printf("  a = %u\n", item->a);
		free(item);
	}
	
	
	
	
}