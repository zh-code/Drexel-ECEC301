#include <stdio.h>
#include <stdlib.h>
#include "mymath.h"

int main (int argc, char** argv)
{
	int i;
	unsigned long* elements;
	unsigned int num_elements;
	
	if (argc != 2) {
		printf ("Usage: %s [nfibs]\n", argv[0]);
	}
	else{
		num_elements = atoi(argv[1]);
	
	
	elements = malloc(num_elements * sizeof(long));
		
	compute_fibs (elements, &num_elements);
		
	printf ("Allocated %u ints [%zu bytes] @ %p\n", num_elements, num_elements * sizeof(long), elements);
		
	for (i=0; i<num_elements; i++) {
		printf ("fibs[%i] @ %p : %lu\n", i, &elements[i], elements[i]);
	}
	printf ("Displaying %u fibs\n", num_elements);
		
	free(elements);
	}
}

