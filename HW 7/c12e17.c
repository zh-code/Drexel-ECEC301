#include <stdio.h>

int sum_two_dimensional_array(const int a[][LEN], int n){
	int *temp, sum = 0;
	for (temp = a[0]; temp < a[0] + n * LEN; temp++)
	sum += *temp;
	return sum;
}