class Question(object):
    def __init__(self, question="", answer=""):
        self.question = question
        self.answer = answer

    def setText(self, question):
        self.question = question

    def setAnswer(self, answer):
        self.answer = answer

    def display(self):
        print self.question

    def checkAnswer(self, answer):
        boo = self.answer == answer
        print boo

class ChoiceQuestion(Question):
    def __init__(self, question, answer):
        super(ChoiceQuestion, self).__init__(question, answer)
        self.choiceList = [[answer, True]]

    def addChoice(self, choice, answer):
        list_temp = [choice, answer]
        self.choiceList.append(list_temp)

    def display(self):
        super(ChoiceQuestion, self).display()
        count = 0
        for item in self.choiceList:
            count += 1
            print "%d. %s" % (count, item[0])

    def checkAnswer(self, number):
        try:
            boo = super(ChoiceQuestion, self).checkAnswer(self.choiceList[number-1][0])
        except TypeError:
            number = int(number)
            boo = super(ChoiceQuestion, self).checkAnswer(self.choiceList[number-1][0])

def presentQuestion(foo):
    foo.display()
    answer = raw_input()
    foo.checkAnswer(answer)


def setQuiz1():
    Q = []
    Q1 = Question("What is the answer for 1+1?", "2")
    Q2 = Question("What is the answer for 1+2?", "3")
    Q3 = Question("What is the answer for 1*1?", "1")
    Q4 = Question("What is the answer for 0*1293123?", "0")
    Q5 = Question("What is your Chinese Name?", "Zhirong Huang")
    Q.append(Q1)
    Q.append(Q2)
    Q.append(Q3)
    Q.append(Q4)
    Q.append(Q5)

    for item in Q:
        presentQuestion(item)
        
def setQuiz2():
    Q = []
    Q1 = ChoiceQuestion("What is the answer for 1+1?", "2")
    Q1.addChoice("3", False)
    Q2 = ChoiceQuestion("What is the answer for 1+2?", "3")
    Q2.addChoice("4", False)
    Q3 = ChoiceQuestion("What is the answer for 1*1?", "1")
    Q3.addChoice("5", False)
    Q4 = ChoiceQuestion("What is the answer for 0*1293123?", "0")
    Q4.addChoice("6", False)
    Q5 = ChoiceQuestion("What is your Chinese Name?", "Zhirong Huang")
    Q5.addChoice("Mark Huang", False)
    Q.append(Q1)
    Q.append(Q2)
    Q.append(Q3)
    Q.append(Q4)
    Q.append(Q5)

    for item in Q:
        presentQuestion(item)

    
    

    
