from Person import *

John = Instructor("John", 1965)
Mark = Student("Mark", 1992)

name = John.getName()
print "Jogn.getName():", name

year = John.getBirthYear()
print "John.getBirthYear():", year

gets = John.getSalary()
print "John.getSalary():", gets

sets = John.setSalary(100)
print "John.setSalary(100):", sets

gets = John.getSalary()
print "(New)John.getSalary():", gets


gets = Mark.getName()
print "Mark.getName():", gets

gets = Mark.getBirthYear()
print "Mark.getBirthYear():", gets

gets = Mark.getGPA()
print "Mark.getGPA():", gets

gets = Mark.getMajor()
print "Mark.getMajor():", gets

sets = Mark.setGPA(0.1)
print "Mark.setGPA(0.1)", sets

sets = Mark.setMajor("EE and Computer Engineering")
print "Mark.setMajor(\"EE and Computer Engineering\"):", sets

gets = Mark.getGPA()
print "Mark.getGPA():", gets

get = Mark.getMajor()
print "Mark.getMajor():", gets

print "Instructor.__repr():\n", John.__repr__()
print "Student.__repr__():\n", Mark.__repr__()