#include <stdio.h>
#include <stdlib.h>

struct node
{
	int value;
	struct node *next;
};

int count_occurrence (struct node *list, int n){
	int count = 0;
	while (list){
		if (list->value == n)
			count++;
			printf("%i", count);
		list = list->next;
	}
	return count;
}

int main (int argc, char** argv)
{
	struct node* item;
	int re;
	item = malloc (sizeof(struct node));
	item->value = 111;
	re = count_occurrence(item, 1);
	printf("%i\n", re);
}