#include<stdio.h>
int main(void){
    int num1, num2, gcd, i;
    printf("Enter a fraction:");
    scanf("%d/%d", &num1, &num2);
    for (i = 2; i <= num1; i++){
	if (num1 % i == 0 && num2 % i == 0)
	    gcd = i;
    }
    num1 = num1 / gcd;
    num2 = num2 / gcd;
    printf("In lowest terms: %d/%d", num1, num2);
}