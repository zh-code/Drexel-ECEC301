class Person(object):
    def __init__(self, name, year):
        self._name = name
        self._year = year

    def getName(self):
        return self._name

    def getBirthYear(self):
        return self._year

    def __repr__(self):
        string = """
        Person Class

        Functions:
        Constructor (name, year):
            set name and year to self

        getName():
            return name assigned to object

        getBirthYear():
            return year assigned to object
        """
        return string

class Student(Person):
    def __init__(self, name, year, major='EE', GPA=4.0):
        super(Student, self).__init__(name, year)
        self._major = major
        self._GPA = GPA

    def setMajor(self, major):
        self._major = major

    def setGPA(self, GPA):
        self._GPA = GPA

    def getMajor(self):
        return self._major

    def getGPA(self):
        return self._GPA

    def __repr__(self):
        string = """
        Student Class inherited from People class
        Person Class

        Functions:
        Constructor (name, year):
            Set name and year to self

        getName():
            Return name assigned to object

        getBirthYear():
            Return year assigned to object

        Student Class:

        Functions:
        Constructor (name, year, major*, GPA*):
            Major and GPA are optional attributes.
            Default values are major='EE' and GPA=4.0

        setMajor(major):
            Set major to self

        setGPA(GPA):
            Set GPA to self

        getMajor():
            Return major assigned to object

        getGPA():
            Return GPA assigned to object
        """

        return string

class Instructor(Person):
    def __init__(self, name, year, salary=70):
        super(Instructor, self).__init__(name, year)
        self._salary = salary

    def setSalary(self, salary):
        self._salary = salary

    def getSalary(self):
        return self._salary

    def __repr__(self):
        string = """
        Student Class inherited from People class
        Person Class

        Functions:
        Constructor (name, year):
            Set name and year to self

        getName():
            Return name assigned to object

        getBirthYear():
            Return year assigned to object

        Instructor Class

        Functions:
        Constructor (name, year, salary*)
            Salary is optional attribute.
            Default value is 70

        setSalary(salary):
            Set salary to object.

        getSalary():
            return salary assigned to object.
        """
        return string
