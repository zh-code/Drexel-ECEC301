#include <stdio.h>
#include <stdlib.h>

int *create_array(int n, int initial_value)
{
	int *a, *b;
	a = malloc(n * sizeof(int));
	if (a != NULL){
		for (b=a; b<a + n; b++){
			*b = initial_value;
		}
	}
	return a;
}

int main (int argc, char** argv)
{
	int i;
	int *ans = create_array(10, 1);
	for (i = 0; i<10; i++){
		printf ("ans[%i] : %i\n", i, ans[i]);
	}
}