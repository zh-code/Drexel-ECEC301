#include <stdio.h>

void store_zeros(int a[], int n){
	int *temp;
	
	for (temp = a; temp <= a + n; temp++){
		*temp = 0;
	}
}