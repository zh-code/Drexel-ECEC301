#include <stdio.h>
int is_prime(int n)
{
	int d;
	for (d=2; (d*d) <= n; d++){
		if (!(n%d))
			return 0;
	}
	
	return 1;
}

int main(void){
	int num, ret;
	for (num = 2; num < 26; num++){
		ret = is_prime(num);
		switch(ret){
			case 0:
				printf("%d is not a prime number.\n", num);
				break;
			case 1:
				printf("%d is a prime number.\n", num);
				break;
		}
	}
	
}