#include <stdlib.h>
#include <stdio.h>
#include "list.h"

int main (int argc, char** argv){
	struct list* head;
	struct list* item;
	
	unsigned int i = 0;
	
	head = malloc (sizeof(struct list));
	
	list_init(head);
	
	for (i = 5; i <= 500; i *= 10){
		item = malloc(sizeof(struct list));
		item->x = i;
		item->y = 2*i;
		list_add_to_head(head, item);
	}
	
	printf ("List contains %u item \n---\n", list_count (head));
	for (i=0, item=head->next; item != NULL; item = item->next){
		printf("Item %u:\n", i++);
		printf("  x = %u\n", item->x);
		printf("  y = %u\n", item->y);
	}
	
	printf ("List contains %u item \n---\n", list_count (head));
	for (i=0, item = list_pop_head(head); item != NULL; item = list_pop_head(head)){
		printf("Item %u:\n", i++);
		printf("  x = %u\n", item->x);
		printf("  y = %u\n", item->y);
		free(item);
	}
	
	printf("List contains %u items\n", list_count(head));
	free(head);
	return 0;
}