from Fraction import *

ans = "{0:.2f}".format(Fraction(1, 2) + Fraction (1, 3))
print '"{0:.2f}".format(Fraction(1, 2) + Fraction (1, 3))'
print ans

ans = "{0:.2f}".format(Fraction(1, 2) - Fraction (1, 3))
print '"{0:.2f}".format(Fraction(1, 2) - Fraction (1, 3))'
print ans

ans = Fraction(1, 2) == Fraction (1, 2)
print 'Fraction(1, 2) == Fraction (1, 2)'
print ans
ans = Fraction(1, 2) == Fraction (1, 3)
print 'Fraction(1, 2) == Fraction (1, 3)'
print ans

ans = Fraction(1, 2) < Fraction (1, 2)
print 'Fraction(1, 2) < Fraction (1, 2)'
print ans
ans = Fraction(1, 4) < Fraction (1, 3)
print ' Fraction(1, 4) < Fraction (1, 3)'
print ans
ans = Fraction(1, 2) < Fraction (1, 3)
print 'Fraction(1, 2) < Fraction (1, 3)'
print ans

ans = Fraction(1, 2) != Fraction (1, 3)
print 'Fraction(1, 2) != Fraction (1, 3)'
print ans
ans = Fraction(1, 2) != Fraction (1, 2)
print 'Fraction(1, 2) != Fraction (1, 2)'
print ans

ans = Fraction(1, 2) <= Fraction (1, 2)
print 'Fraction(1, 2) <= Fraction (1, 2)'
print ans
ans = Fraction(1, 4) <= Fraction (1, 3)
print "Fraction(1, 4) <= Fraction (1, 3)"
print ans
ans = Fraction(1, 2) <= Fraction (1, 3)
print 'Fraction(1, 2) <= Fraction (1, 3)'
print ans

ans = Fraction(1, 2) > Fraction (1, 2)
print 'Fraction(1, 2) > Fraction (1, 2)'
print ans
ans = Fraction(1, 4) > Fraction (1, 3)
print 'Fraction(1, 4) > Fraction (1, 3)'
print ans
ans = Fraction(1, 2) > Fraction (1, 3)
print 'Fraction(1, 2) > Fraction (1, 3)'
print ans

ans = Fraction(1, 2) >= Fraction (1, 2)
print 'Fraction(1, 2) >= Fraction (1, 2)'
print ans
ans = Fraction(1, 4) >= Fraction (1, 3)
print 'Fraction(1, 4) >= Fraction (1, 3)'
print ans
ans = Fraction(1, 2) >= Fraction (1, 3)
print 'Fraction(1, 2) >= Fraction (1, 3)'
print ans

ans = float(Fraction(1, 2))
print 'float(Fraction(1, 2))'
print ans
ans = repr(Fraction(1, 2))
print 'repr(Fraction(1, 2))'
print ans