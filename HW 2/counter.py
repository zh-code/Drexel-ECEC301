class CharCounter(object):
    def __init__(self, name, length):
        self.name = name
        self.length = length
        self.n = 0
        
    def __iter__(self):
        self.list = []
        self.iter_return = []
        self.count = 0
        f = open(self.name)
        for line in f:
            sp = line.split(" ")
            for item in sp:
                item = item.rstrip().strip(",.() ")
                self.list.append(item)
                
        for item in self.list:
            if len(item) == 11:
                self.iter_return.append(item)
        return self
    

    def next(self):
        try:
            ret = self.iter_return[self.count]
        except IndexError:
            raise StopIteration
        self.count += 1
        return ret
                
    def gen(n):
        i = 0
        while i < len(self.iter_return)
            yield self.iter_return[i]
            i += 1

if __name__ == "__main__":
    for word in CharCounter('agency.txt', 11):
        print "'%s'" % word
    
        
