#include <stdio.h>


struct fraction{
       int numerator,denominator ;
       };
	   
int gcd(int a, int b)
{
	if( a==b)
		return a; 
	else 
		if (a>b) 
			return gcd(a-b,b);
		else 
			return gcd(a,b-a);
}

struct fraction reduce(struct fraction f)
{
	int m;
	m = gcd(f.numerator, f.denominator );
	f.numerator = f.numerator/m;
	f.denominator  = f.denominator/m;
	return f;
}

struct fraction add(struct fraction fraction_1,struct fraction fraction_2)
{
	int n,d,m;struct fraction fraction_3;
	n=fraction_1.numerator*fraction_2.denominator  + fraction_2.numerator*fraction_1.denominator ;
	d=fraction_1.denominator  * fraction_2.denominator ;
	m=gcd(n,d);
	n=n/m;
	d=d/m;
	fraction_3.numerator = n;
	fraction_3.denominator  = d;
	return fraction_3;
}

struct fraction subtract(struct fraction fraction_1,struct fraction fraction_2)
{
	int n,d,m;struct fraction fraction_3;
	n=fraction_1.numerator*fraction_2.denominator  - fraction_2.numerator*fraction_1.denominator ;
	d=fraction_1.denominator  * fraction_2.denominator ;
	if(n!=0){
		m=gcd(n,d);
		n=n/m;
		d=d/m;
	}
	fraction_3.numerator = n;
	fraction_3.denominator  = d;
	return fraction_3;
}

struct fraction multiply(struct fraction fraction_1,struct fraction fraction_2)
{
	int n,d,m;struct fraction fraction_3;
	fraction_3.numerator = fraction_1.numerator *fraction_2.numerator;
	fraction_3.denominator  = fraction_1.denominator  * fraction_2.denominator ;
	m=gcd(fraction_3.numerator,fraction_3.denominator );
	fraction_3.numerator /=m;
	fraction_3.denominator  /=m;
	return fraction_3;
}

struct fraction divide(struct fraction fraction_1,struct fraction fraction_2)
{
	int n,d,m;struct fraction fraction_3;
	fraction_3.numerator = fraction_1.numerator *fraction_2.denominator ;
	fraction_3.denominator  = fraction_1.denominator  * fraction_2.numerator;
	m=gcd(fraction_3.numerator,fraction_3.denominator );
	fraction_3.numerator /=m;
	fraction_3.denominator  /=m;
	return fraction_3;
}

int main (int argc, char** argv)
{
	struct fraction f1, f2;
	f1.numerator = 2;
	f1.denominator = 4;
	
	f2.numerator = 3;
	f2.denominator = 7;
	
	struct fraction a, b, c, d, e;
	a = reduce(f1);
	printf ("Part a: reduce f1\n");
	printf ("%i/%i\n\n", a.numerator, a.denominator);
	
	printf("Part b: add f1 and f2:\n");
	b = add(f1, f2);
	printf ("%i/%i\n\n", b.numerator, b.denominator);
	
	printf("Part c: subtract f2 from f1:\n");
	c = subtract(f1, f2);
	printf ("%i/%i\n\n", c.numerator, c.denominator);
	
	printf("Part d: multiply f1 and f2:\n");
	d = multiply(f1, f2);
	printf ("%i/%i\n\n", d.numerator, d.denominator);
	
	printf("Part e: divide f1 from f2:\n");
	e = divide(f2, f1);
	printf ("%i/%i\n\n", e.numerator, e.denominator);
	
	
	
	
}

