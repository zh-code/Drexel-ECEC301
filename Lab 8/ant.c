#include <stdio.h>
#include "ant.h"

void init_ant (struct ant* a, int position){
	a->position = position;
	a->direction = 1;
}
void ant_turn (struct ant* a){
	a->direction = -1 * a->direction;
}
void ant_move (struct ant* a, unsigned int distance){
	a->position = a->position + distance * a->direction;
}
int ant_get_position (struct ant* a){
	return a->position;
}
char* ant_get_direction (struct ant* a){
	if (a->direction > 0){
		return "Right";
	}
	else {
		return "Left";
	}
}
