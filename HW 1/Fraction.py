class Fraction(object):
    def __init__(self, numerator, denominator):
        self._numerator = numerator
        self._denominator = denominator
        self._rational = numerator/float(denominator)

    def __add__(self, right):
        ans = self._rational + right._rational
        return ans

    def __sub__(self, right):
        ans = self._rational - right._rational
        return ans

    def __eq__(self, right):
        ans = self._rational == right._rational
        return ans

    def __lt__(self, right):
        ans = self._rational < right._rational
        return ans

    def __ne__(self, right):
        ans = self._rational != right._rational
        return  ans

    def __le__(self, right):
        if self.__eq__(right):
            ans = True
        else:
            ans = self._rational < right._rational
        return ans

    def __gt__(self, right):
        ans = self._rational > right._rational
        return ans

    def __ge__(self, right):
        if self.__eq__(right):
            ans = True
        else:
            ans = self._rational > right._rational
        return ans

    def __float__(self):
        ans = float(self._rational)
        return ans

    def __repr__(self):
        string = """
        Fraction Class

        Functions:
        __init__(numerator, denominator)
            Construct a rational number with a given numerator and denominator

        __add__()
            Add two Fraction instances

        __sub__():
            Substract two Fraction instances

        __eq__():
            Check if two Fraction instances are equal.
            Returns boolean value

        __lt__():
            Check if one Fraction instance is less than the other
            Returns boolean value:
                Left = Right : False
                Left < Right : True
                Left > Right : False

        __ne__():
            Check if two Fraction instances are not equal.
            Returns boolean value

        __le__():
            Check if one Fraction instance is less than or equal to the other
            Returns boolean value:
                Left = Right : True
                Left < Right : True
                Left > Right : False

        __gt__():
            Check if one Fraction instance is greater than the other.
            Returns boolean value

        __ge__():
            Check if one Fraction instance is greater than or equal to the other
            Returns boolean value:
                Left = Right : True
                Left < Right : False
                Left > Right : True

        __float__():
            Gets a floating point representation of a Fraction instance.
            Returns float
            """

        return string

