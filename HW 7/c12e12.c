#include <stdio.h>

void find_two_largest(const int *a, int n, int *largest, int *second_largest){
	int *temp;
	if (*a[0] > *a[1]){
		*largest = *a[0];
		*second_largest = *a[1];
	}
	else{
		*largest = *a[1];
		*second_largest = *a[2];
	}
	
	for (temp=2; temp<n; temp++){
		if (*largest < a[temp]){
			*second_largest = *largest;
			*largest = *a[temp];
		}
		else if (*second_largest < a[temp]){
			*second_largest = *a[temp];
		}
	}
	
}