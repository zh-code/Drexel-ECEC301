class Question(object):
    def __init__(self, question="", answer=""):
        self.question = question
        self.answer = answer

    def setText(self, question):
        self.question = question

    def setAnswer(self, answer):
        self.answer = answer

    def display(self):
        print self.question

    def checkAnswer(self, answer):
        boo = self.answer == answer
        print boo

class ChoiceQuestion(Question):
    def __init__(self):
        super(ChoiceQuestion, self).__init__()
        self.choiceList = []

    def __iter__(self):
        self.count = 0
        return self

    def next(self):
        try:
            ret = self.choiceList[self.count]
        except IndexError:
            raise StopIteration

        self.count += 1
        return ret

    def addChoice(self, choice, answer=False):
        list_temp = [choice, answer]
        self.choiceList.append(list_temp)

    def checkAnswer(self, number):
        number = int(number)
        if self.choiceList[number-1][1]:
            print "True"
        else:
            print "False"
        
def presentQuestion(foo):
    foo.display()
    if isinstance(foo, ChoiceQuestion):
        for i, choice in enumerate(foo):
            print '%i: %s' % (i+1, choice[0])
    answer = raw_input()
    foo.checkAnswer(answer)
