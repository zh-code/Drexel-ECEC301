#include <stdio.h>
int main(void)
{
	int income;
	double tax;
	printf("Enter incomeable income: ");
	scanf("%d", &income);
	switch(income){
		case 0 ... 750:
			tax = 0.01 * income;
			printf("Tax due: %.2f\n", tax);
			break;
		case 751 ... 2250:
			tax = 7.5 + 0.02 * (income - 750);
			printf("Tax due: %.2f\n", tax);
			break;
		case 2251 ... 3750:
			tax = 37.5 + 0.03 * (income - 2250);
			printf("Tax due: %.2f\n", tax);
			break;
		case 3751 ... 5250:
			tax = 82.5 + 0.04 * (income - 3750);
			printf("Tax due: %.2f\n", tax);
			break;
		case 5251 ... 7000:
			tax = 142.5 + 0.05 * (income - 5250);
			printf("Tax due: %.2f\n", tax);
			break;
		default :
			tax = 230 + 0.06 * (income - 7000);
			printf("Tax due: %.2f\n", tax);
			break;
	}
}