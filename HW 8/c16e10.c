#include <stdio.h>
#include<stdbool.h>

struct point {int x, y;};
struct rectangle {struct point upper_left, lower_right;};

int area (struct rectangle r)
{
	int h, l, a;
	l = r.lower_right.x - r.upper_left.x;
	h = r.upper_left.y - r.lower_right.y;
	a = h * l;
	return a;
}

struct point center (struct rectangle r)
{
	struct point c;
	c.x = r.lower_right.x - r.upper_left.x;
	c.y = r.upper_left.y - r.lower_right.y;
	return c;
}

struct rectangle move (struct rectangle r, int x, int y)
{
	struct rectangle r1 = r;
	r1.upper_left.x += x;
	r1.lower_right.x += x;
	r1.upper_left.y += y;
	r1.lower_right.y += y;
	return r1;
}

bool inside (struct rectangle r, struct point p)
{
	return r.upper_left.x <= p.x && r.lower_right.x >= p.x && r.upper_left.y <= p.y && p.y <= r.lower_right.y;
}

int main (int argc, char** argv)
{
	struct point p_upperleft, p_lower_right, p;
	struct rectangle r, r_move;
	int a;
	bool b;
	
	p_upperleft.x = 1;
	p_upperleft.y = 3;
	p_lower_right.x = 3;
	p_lower_right.y = 1;
	
	r.upper_left = p_upperleft;
	r.lower_right = p_lower_right;
	a = area(r);
	printf ("Area: %i\n", a);
	p = center(r);
	printf ("Center, x: %i; y: %i\n", p.x, p.y);
	r_move = move (r, 1, 1);
	printf ("Move: upperleft.x: %i, upperleft.y: %i, lowerright.x: %i, lowerright.y: %i\n", r_move.upper_left.x, r_move.upper_left.y, r_move.lower_right.x, r_move.lower_right.y);
	p.x = 5;
	p.y = 7;
	b = inside (r, p);
	if (b)
		printf("The point (%i, %i) is inside the rectangle\n", p.x, p.y);
	else
		printf("The point (%i, %i) is not inside the rectangle\n", p.x, p.y);
}