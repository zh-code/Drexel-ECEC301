#ifndef _list_h_
#define _list_h_
struct list {
	unsigned int x;
	unsigned int y;
	
	struct list* next;
};

void list_init (struct list* head);
unsigned int list_count (struct list* head);
void list_add_to_head (struct list* head, struct list* new_item);
struct list* list_pop_head (struct list* head);

#endif